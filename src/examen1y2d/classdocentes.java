/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen1y2d;

/**
 *
 * @author Martinez
 */
public class classdocentes {
    
    private String numerod;
    private String Nombre;
    private String domicilio;
    private float nivel;
    private float pagobh;
    private int horas;
    
    public classdocentes(){
    this.numerod="";
    this.Nombre="";
    this.domicilio="";
    this.nivel=0.0f;
    this.pagobh=0.0f;
    this.horas=0;
    }

    public classdocentes(String numerod, String Nombre, String domicilio, float nivel, float pagobh, int horas) {
    
        this.numerod=numerod;
        this.Nombre = Nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagobh = pagobh;
        this.horas = horas;
    }

    public String getNumerod() {
        return numerod;
    }

    public void setNumerod(String numerod) {
        this.numerod = numerod;
    }

   

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getNivel() {
        return nivel;
    }

    public void setNivel(float nivel) {
        this.nivel = nivel;
    }

    public float getPagobh() {
        return pagobh;
    }

    public void setPagobh(float pagobh) {
        this.pagobh = pagobh;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }
    
    public float calcularP(){
        
        float pagin=this.pagobh*this.horas;
        float aumentoN=pagin*this.nivel;
        float pagoNivel=pagin+aumentoN;
        return pagoNivel;
        
    }
    
    public float decuentoI(){
        
       float pagoim=(float) (this.calcularP()*.16);
       
        return pagoim;
    }
    
    public float totalPago(){
        float totalpago=this.calcularP()-this.decuentoI();
    return totalpago;
    }

  
}
